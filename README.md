![IMAGE](https://bytebucket.org/hippeelee/stars-and-stripes/raw/d90c0c3f170feee29335770a4ad58e0145a8f281/AmericanFlag.png)

# README #

1. Use this sketch file to make modifications to your American Flag.
2. Use the exported American Flag 

### How do I get set up? ###

* [Buy Sketch](http://bohemiancoding.com/sketch/)
* Open File
* Edit file with sketch to suite needs

### Who do I talk to? ###

* [Matt](http://hippeelee.com/hi)

## here is a pay it forward license attributed to this work. That is, **find a way** to give something of equal or greater value to a community in the digital realm or the real world. 